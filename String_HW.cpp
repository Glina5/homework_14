﻿#include <iostream>
#include <string>

int main()
{
    std::string First{ "Hello World!" };

    std::cout << "Enter a string or leave a string empty: ";
   
    std::getline(std::cin, First);    
    
    //Заполнить строку по-умолчанию
    if (First == "\0")
    {
        First = "Hello World!";
    }

    //Вывести саму строковую переменную
    std::cout << First << "\n";

    //Вывести длину строки
    std::cout << "String length: " << First.length() << "\n";
    
    //Вывести первый символы этой строки
    std::cout << "First char: " << First[0] << "\n";

    //Вывести последний символы этой строки
    std::cout << "Last char: " << First[(First.length()-1)] << "\n";

}

